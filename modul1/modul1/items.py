# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class HotlineItem(scrapy.Item):
    name = scrapy.Field()
    price = scrapy.Field()
    url = scrapy.Field()
    image_urls = scrapy.Field()
    description = scrapy.Field()
   

class HotlineShop(scrapy.Item):
    shop_name=scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()