

ВАРІАНТ 12

import scrapy
from modul1.items import HotlineItem, HotlineShop


class HotlineSpider(scrapy.Spider):
    name = "hotline"
    allowed_domains = ["hotline.ua"]
    start_urls = [f"https://hotline.ua/ua/bt/stiralnye-i-sushilnye-mashiny/?p={page}" for page in range(1, 3)]

    def parse(self, response):
        items = response.css('div.list-body__content').css('.list-item')
        for item in items:
            name = item.css('a.text-md.link.link--black::text').get()
            url = item.css('a.text-md.link.link--black::attr(href)').get()
            price = item.css('.list-item__value-price.text-md.text-orange.text-lh--1::text').get()

            if price:
                price = price.strip().replace('\xa0', '')  
                price = int(price)

            image_url = item.css('img::attr(src)').get()
            description = item.css('span.spec-item--bullet::text').get()  # Опис товару

            yield HotlineItem(
                name=name.strip(),
                price=price,
                url=f"https://hotline.ua{url}",  
                image_urls=[f"https://hotline.ua{image_url}"],
                description=description.strip() if description else None  
            )

            if url:
                yield scrapy.Request(f"https://hotline.ua{url}", callback=self.parse_shops, meta={'name': name, 'price': price})

    def parse_shops(self, response):
        name = response.meta.get('name')
        price = response.meta.get('price')

        shop_items = response.css('a.shop__title::text').getall()
        for shop_name in shop_items:
            if shop_name:
                shop_name = shop_name.strip()

            yield HotlineShop(
                shop_name=shop_name,
                name=name.strip(),  
                price=price
            )
